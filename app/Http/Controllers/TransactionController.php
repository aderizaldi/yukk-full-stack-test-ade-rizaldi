<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Enums\TransactionType;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    //transaction page
    public function transaction()
    {
        return view('user.pages.transaction');
    }

    //history transaction page
    public function history()
    {
        $transactions = User::find(auth()->user()->id)->transactions();

        //filter
        if (request()->has('type') && request()->type != '') {
            $transactions = $transactions->where('type', request()->type);
        }
        if (request()->has('start_date') && request()->start_date != '') {
            $transactions = $transactions->whereDate('created_at', '>=', date('Y-m-d', strtotime(request()->start_date)));
        }
        if (request()->has('end_date') && request()->end_date != '') {
            $transactions = $transactions->whereDate('created_at', '<=', date('Y-m-d', strtotime(request()->end_date)));
        }

        //search
        if (request()->has('search') && request()->search != '') {
            $transactions = $transactions->where('description', 'like', '%' . request()->search . '%')->orWhere('transaction_id', 'like', '%' . request()->search . '%');
        }

        $transactions = $transactions->orderBy('created_at', 'desc')->paginate(5)->withQueryString();
        return view('user.pages.transaction-history', compact('transactions'));
    }

    //transaction post
    public function transactionPost(Request $request)
    {
        $request->validate([
            'type' => 'required|in:' . implode(',', TransactionType::getValues()),
            'amount' => 'required|numeric|min:1',
            'description' => 'nullable|string|max:255',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $transaction_type = new TransactionType($request->type);

        if ($transaction_type == TransactionType::Topup) {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        }

        DB::beginTransaction();
        $user = User::find(auth()->user()->id);
        $user = $user->transaction($transaction_type, $request->amount, $request->description, $request->image?->store('images/transaction', 'public'));

        if ($user->balance < 0) {
            DB::rollBack();
            return back()->with('error', 'Insufficient balance');
        }

        DB::commit();
        return back()->with('success', 'Transaction successful');
    }
}
