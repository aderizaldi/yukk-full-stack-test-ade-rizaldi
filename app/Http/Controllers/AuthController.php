<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //login page
    public function login()
    {
        return view('auth.pages.login');
    }

    //register page
    public function register()
    {
        return view('auth.pages.register');
    }

    //register post
    public function registerPost(Request $request)
    {
        //validate request
        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:users',
            'password' => 'required',
        ]);

        //create user
        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => Hash::make($request->password),
        ]);

        //login user
        Auth::login($user);

        //redirect to home
        return redirect()->route('home');
    }

    //login post
    public function loginPost(Request $request)
    {
        //validate request
        $request->validate([
            'username' => 'required',
            'password' => 'required',
            'remember' => 'nullable',
        ]);

        //attempt login
        if (Auth::attempt($request->only('username', 'password'), $request->remember ? true : false)) {
            //redirect to home
            return redirect()->route('home');
        }

        //redirect back
        return back()->with('error', 'Invalid credentials');
    }

    //logout
    public function logout()
    {
        //logout user
        Auth::logout();

        //redirect to login
        return redirect()->route('login');
    }
}
