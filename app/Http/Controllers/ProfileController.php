<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //profile page
    public function profile()
    {
        return view('user.pages.profile');
    }

    //profile post
    public function profilePut(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|unique:users,username,' . auth()->user()->id,
        ]);

        $user = User::find(auth()->user()->id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->save();

        return back()->with('success', 'Profile updated');
    }

    //change password post
    public function passwordPut(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'password' => 'required',
        ]);

        $user = User::find(auth()->user()->id);

        if (!password_verify($request->current_password, $user->password)) {
            return back()->with('error', 'Current password is incorrect');
        }

        $user->password = bcrypt($request->password);
        $user->save();

        return back()->with('success', 'Password updated');
    }
}
