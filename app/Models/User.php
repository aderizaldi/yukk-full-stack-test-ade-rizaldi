<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Str;
use App\Enums\TransactionType;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'password' => 'hashed',
        ];
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function transaction(TransactionType $transaction_type, int $amount, $description = '', $image = null)
    {
        $balance = $this->balance + $transaction_type->calculate($amount);

        $this->transactions()->create([
            'transaction_id' => $transaction_type->value . '-' . Str::uuid(),
            'type' => $transaction_type->value,
            'amount' => $amount,
            'balance_before' => $this->balance,
            'balance_after' => $balance,
            'description' => $description,
            'image' => $image,
        ]);

        $this->balance = $balance;
        $this->save();

        return $this;
    }
}
