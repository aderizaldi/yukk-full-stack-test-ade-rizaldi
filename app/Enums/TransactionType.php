<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Transaction()
 * @method static static Topup()
 */
final class TransactionType extends Enum
{
    const Transaction = 'transaction';
    const Topup = 'topup';

    public function calculate(int $amount): int
    {
        return match ($this->value) {
            self::Transaction => -$amount,
            self::Topup => $amount,
        };
    }
}
