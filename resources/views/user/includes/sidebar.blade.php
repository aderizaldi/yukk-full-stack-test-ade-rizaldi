<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo ">
        <a href="#" class="app-brand-link">
            <span class="app-brand-text demo menu-text fw-semibold ms-2">YUKK TEST</span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <i class="mdi menu-toggle-icon d-xl-block align-middle mdi-20px"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <li class="menu-item {{ request()->is('/') ? 'active' : '' }}">
            <a href="{{ route('home') }}" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-view-dashboard-outline"></i>
                <div>Transaction</div>
            </a>
        </li>
        <li class="menu-item {{ request()->is('history*') ? 'active' : '' }}">
            <a href="{{ route('history') }}" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-view-dashboard-outline"></i>
                <div>History Transaction</div>
            </a>
        </li>
        <li class="menu-header fw-medium mt-5">
            <span class="menu-header-text">Settings</span>
        </li>
        <li class="menu-item {{ request()->is('profile*') ? 'active' : '' }}">
            <a href="{{ route('profile') }}" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-account-outline"></i>
                <div>Profile</div>
            </a>
        </li>
        <li class="menu-item">
            <a href="#" class="menu-link text-danger" data-bs-toggle="modal" data-bs-target="#logoutModal">
                <i class="menu-icon tf-icons mdi mdi-logout"></i>
                <div>Logout</div>
            </a>
        </li>
</aside>
