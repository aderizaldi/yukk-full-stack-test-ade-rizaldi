@extends('user.layouts.main')
@use('App\Enums\TransactionType')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            {{-- card to show user balance --}}
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title fs-5 p-0 m-0">Balance</h5>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h2 class="card-title">Rp.
                                    {{ number_format(auth()->user()->balance, 0, ',', '.') }}
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title fs-5 p-0 m-0">Transaction</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('home.post') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                            <div class="mt-2" id="formTransaction">
                                <div class="form-floating form-floating-outline form-floating-bootstrap-select mb-2">
                                    <select class="selectpicker w-100" data-style="btn-default" name="type" required
                                        placeholder="" id="typeTransaction">
                                        @foreach (TransactionType::getInstances() as $type)
                                            <option value="{{ $type->value }}" @selected(old('type') == $type)>
                                                {{ $type->key }}</option>
                                        @endforeach
                                    </select>
                                    <label>Type</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-2">
                                    <input type="number" class="form-control" name="amount" required placeholder=""
                                        value={{ old('amount') }}>
                                    <label>Amount</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-2">
                                    <textarea class="form-control h-px-100" placeholder="" name="description">{{ old('description') }}</textarea>
                                    <label>Description (optional)</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-2" id="imageUpload"
                                    style="display:none;">
                                    <input type="file" class="form-control" name="image" value="{{ old('image') }}">
                                    <label>Proof of Transaction</label>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        if ($("#typeTransaction").val() == 'topup') {
            //show imageUpload
            $('#imageUpload').show();
        } else {
            //hide and remove value imageUpload
            $('#imageUpload').hide();
            $('#imageUpload input').val('');
        }

        $('#typeTransaction').on('change', function() {
            //if value = topup then add input image, else remove input image
            if ($(this).val() == 'topup') {
                //show imageUpload
                $('#imageUpload').show();
            } else {
                //hide and remove value imageUpload
                $('#imageUpload').hide();
                $('#imageUpload input').val('');
            }
        });
    </script>
@endpush
