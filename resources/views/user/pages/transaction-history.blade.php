@extends('user.layouts.main')
@use('App\Enums\TransactionType')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            {{-- card to show user balance --}}
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title fs-5 p-0 m-0">Balance</h5>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h2 class="card-title">Rp.
                                    {{ number_format(auth()->user()->balance, 0, ',', '.') }}
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title fs-5 p-0 m-0">Transaction History</h5>
                    </div>
                    <div class="card-body">
                        {{-- filter using type, start date and end date --}}
                        <div class="row gy-3">
                            <div class="col-12 mb-4">
                                <form action="{{ route('history') }}" method="GET">
                                    <div class="row gy-3">
                                        <div class="col-4">
                                            <label for="type" class="form-label">Type</label>
                                            <select name="type" class="form-select">
                                                <option value="">All</option>
                                                @foreach (TransactionType::getKeys() as $type)
                                                    <option value="{{ TransactionType::getValue($type) }}"
                                                        {{ request()->get('type') == TransactionType::getValue($type) ? 'selected' : '' }}>
                                                        {{ $type }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <label for="start_date" class="form-label">Start Date</label>
                                            <input type="text" placeholder="DD/MM/YYYY" data-provide="datepicker"
                                                name="start_date" class="form-control datepicker"
                                                value="{{ request()->get('start_date') }}">
                                        </div>
                                        <div class="col-3">
                                            <label for="end_date" class="form-label">End Date</label>
                                            <input type="text" placeholder="DD/MM/YYYY" data-provide="datepicker"
                                                name="end_date" class="form-control datepicker"
                                                value="{{ request()->get('end_date') }}">
                                        </div>
                                        <div class="col-2 d-flex align-items-end">
                                            <button type="submit" class="btn btn-primary w-100">Filter</button>
                                        </div>
                                        <div class="col-12">
                                            {{-- search --}}
                                            <div class="input-group">
                                                <input type="text" name="search" class="form-control"
                                                    placeholder="Search" value="{{ request()->get('search') }}">
                                                <button type="submit" class="btn btn-primary">Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @foreach ($transactions as $transaction)
                                <div class="col-12">
                                    {{-- make card with header type transaction and date created and body amount, description, balance before, balance after --}}
                                    <div class="card">
                                        <div class="card-header pb-2">
                                            <div class="d-flex justify-content-between">
                                                <h5 class="card-title fs-5 p-0 m-0">
                                                    {{ TransactionType::getKey($transaction->type) }}
                                                </h5>
                                                <span class="text-muted fs-6">
                                                    {{ $transaction->created_at->format('d M Y H:i') }}
                                                </span>
                                            </div>
                                            <span class="text-muted fs-6">Transaction ID :
                                                {{ $transaction->transaction_id }}</span>
                                        </div>
                                        <div class="card-body">
                                            <div class="d-flex justify-content-between">
                                                <h2 class="card-title">
                                                    Rp. {{ number_format($transaction->amount, 0, ',', '.') }}
                                                </h2>
                                            </div>
                                            <div class="d-flex flex-column">
                                                <span class="text-muted fs-6">Description:
                                                    {{ $transaction->description }}</span>
                                            </div>
                                            @if ($transaction->type === TransactionType::getValue('Topup'))
                                                <div class="row">
                                                    <div class="col-4">
                                                        <span class="text-primary fs-6">Balance Before: Rp.
                                                            {{ number_format($transaction->balance_before, 0, ',', '.') }}</span>
                                                    </div>
                                                    <div class="col-4">
                                                        <span class="text-primary fs-6">Balance After: Rp.
                                                            {{ number_format($transaction->balance_after, 0, ',', '.') }}</span>
                                                    </div>
                                                    <div class="col-4">
                                                        <span class="text-primary fs-6">Proof of Transaction: <a
                                                                href="" data-bs-toggle="modal"
                                                                data-bs-target="#image-{{ $transaction->id }}"><i
                                                                    class="mdi mdi-eye-circle-outline"></i>Open</a>
                                                        </span>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="row">
                                                    <div class="col-4">
                                                        <span class="text-primary fs-6">Balance Before: Rp.
                                                            {{ number_format($transaction->balance_before, 0, ',', '.') }}</span>
                                                    </div>
                                                    <div class="col-4">
                                                        <span class="text-primary fs-6">Balance After: Rp.
                                                            {{ number_format($transaction->balance_after, 0, ',', '.') }}</span>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    {{-- card footer with pagination bootstrap --}}
                    <div class="card-footer">
                        <div class="d-flex flex-column align-items-center">
                            @include('components.pagination', ['paginator' => $transactions])
                            <span class="text-muted fs-6">Showing {{ $transactions->firstItem() }} to
                                {{ $transactions->lastItem() }} of {{ $transactions->total() }} entries</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal image --}}
    @foreach ($transactions as $transaction)
        @if ($transaction->image)
            <div class="modal fade" id="image-{{ $transaction->id }}" tabindex="-1" role="dialog"
                aria-labelledby="image-{{ $transaction->id }}" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <img src="{{ asset('storage/' . $transaction->image) }}" class="img-fluid"
                            alt="Proof of Transaction">
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endsection
@push('scripts')
    <script>
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom',
            templates: {
                leftArrow: '<i class="mdi mdi-chevron-left"></i>',
                rightArrow: '<i class="mdi mdi-chevron-right"></i>'
            },
            clearBtn: true
        })
    </script>
@endpush
