@extends('auth.layouts.main')
@section('content')
    <div class="authentication-inner row m-0">

        <!-- /Left Text -->
        <div class="d-none d-lg-flex col-lg-7 col-xl-8 align-items-center justify-content-center p-5 pb-2">
            <div>
                <img src="{{ asset('assets/img/illustrations/boy-with-laptop-light.png') }}"
                    class="authentication-image-model d-none d-lg-block" alt="auth-model">
            </div>
        </div>
        <!-- /Left Text -->

        <!-- Register -->
        <div
            class="d-flex col-12 col-lg-5 col-xl-4 align-items-center authentication-bg position-relative py-sm-5 px-4 py-4">
            <div class="w-px-400 mx-auto pt-5 pt-lg-0">
                <h4 class="mb-2">Adventure starts here 🚀</h4>
                <p class="mb-4">Lets create your account now!</p>

                <form class="mb-3" action="{{ route('register.post') }}" method="POST">
                    @csrf
                    @method('POST')
                    <div class="form-floating form-floating-outline mb-3">
                        <input type="text" class="form-control" id="name" name="name"
                            placeholder="Enter your name" autofocus value="{{ old('name') }}" required>
                        <label for="name">Name</label>
                    </div>
                    <div class="form-floating form-floating-outline mb-3">
                        <input type="text" class="form-control" id="username" name="username"
                            placeholder="Enter your username" value="{{ old('username') }}" required>
                        <label for="username">Username</label>
                    </div>
                    <div class="mb-3 form-password-toggle">
                        <div class="input-group input-group-merge">
                            <div class="form-floating form-floating-outline">
                                <input type="password" id="password" class="form-control" name="password"
                                    placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                    aria-describedby="password" required />
                                <label for="password">Password</label>
                            </div>
                            <span class="input-group-text cursor-pointer"><i class="mdi mdi-eye-off-outline"></i></span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary d-grid w-100">
                        Sign up
                    </button>
                </form>

                <p class="text-center mt-2">
                    <span>Already have an account?</span>
                    <a href="{{ route('login') }}">
                        <span>Sign in instead</span>
                    </a>
                </p>
            </div>
        </div>
        <!-- /Register -->
    </div>
@endsection
