
# Yukk Skill Test

This project created using framework Laravel 11.

## Prerequisites

Make sure you have the following installed on your system:

- PHP >= 8.2
- Composer
- Git
## Installation
### Clone the project
Clone the project using git

```bash
  git clone https://gitlab.com/aderizaldi/yukk-full-stack-test-ade-rizaldi
```

Go to the project directory

```bash
  cd yukk-full-stack-test-ade-rizaldi
```

### Set up environment variables

Duplicate the .env.example file and rename it to .env. Update the necessary environment variables like database credentials, etc.

### Install depedencies and setup the project
Install dependencies

```bash
  composer install
```

Generate key

```bash
  php artisan key:generate
```

Create symbolic link to storage

```bash
  php artisan storage:link
```

Create database and seeder
```bash
  php artisan migrate --seed
```

### Run the project locally

Start the server

```bash
  php artisan serve
```
